package eschooldaoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EschoolDaoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EschoolDaoApiApplication.class, args);
	}

}
